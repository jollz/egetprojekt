//
//  RegistrationController.swift
//  BagskytteRaknevark
//
//  Created by dronnefjord on 2015-04-24.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

import UIKit

class RegistrationController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource {
    
    @IBOutlet weak var scoresPickerView: UIPickerView!
    @IBOutlet weak var endsTableView: UITableView!
    @IBOutlet weak var totalScoreLabel: UILabel!
    @IBOutlet weak var arrowAverageLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var style = 0
    var distance = 0
    
    var ends:[String] = []
    var totalScore = 0
    var arrowAverage = 0.0
    var arrowsFired = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scoresPickerView.delegate = self
        scoresPickerView.dataSource = self
        endsTableView.dataSource = self
        totalScoreLabel.text = String(totalScore)
        arrowAverageLabel.text = NSString(format: "%.1f", arrowAverage)
        distanceLabel.text = NSString(format: "%dm", Data.Static.distances[distance])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 3
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Data.Static.scores.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return NSString(format: "%d", Data.Static.scores[row])
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("EndCell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.textAlignment = NSTextAlignment.Center
        cell.textLabel?.text = ends[indexPath.row]
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ends.count
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func ExitRegistration(sender: AnyObject) {
        navigationController?.popToRootViewControllerAnimated(true)
    }
    var counter = 0
    @IBAction func saveEnd(sender: AnyObject) {
        switch(style){
        case 0:
            registerEnd()
            saveData()
            break;
        case 1:
            if(counter < 10){
                registerEnd()
                counter++
                if(counter == 10){
                    var button = sender as UIButton
                    button.setTitle("Spara", forState: UIControlState.Normal)
                }
            } else {
                saveData()
                navigationController?.popToRootViewControllerAnimated(true)
            }
            break;
        case 2:
            if(counter < 20){
                registerEnd()
                counter++
                if(counter == 20){
                    var button = sender as UIButton
                    button.setTitle("Spara", forState: UIControlState.Normal)
                }
            } else {
                saveData()
                navigationController?.popToRootViewControllerAnimated(true)
            }
            break;
        default:
            registerEnd()
            break;
        }
    }
    
    func registerEnd(){
        var score1 = scoresPickerView.selectedRowInComponent(0)
        var score2 = scoresPickerView.selectedRowInComponent(1)
        var score3 = scoresPickerView.selectedRowInComponent(2)
        var totalScoreForEnd = score1+score2+score3
        ends.append(NSString(format: "%d - %d - %d", score1,score2,score3))
        endsTableView.reloadData()
        totalScore += totalScoreForEnd
        totalScoreLabel.text = String(totalScore)
        arrowsFired += 3
        arrowAverage = Double(totalScore)/Double(arrowsFired)
        arrowAverageLabel.text = NSString(format: "%.1f", arrowAverage)
    }
    
    func saveData(){
        switch(style){
        case 0:
                Data.Static.addScore(distance, amount:totalScore)
                Data.Static.addFiredArrows(distance, amount: arrowsFired)
                Data.Static.calculateNewAverage(distance)
                Data.Static.saveData()
            break;
        case 1:
            Data.Static.addScore(distance, amount:totalScore)
            Data.Static.addFiredArrows(distance, amount: arrowsFired)
            Data.Static.calculateNewAverage(distance)
            Data.Static.addAndCompareLatestScore(distance, style: style, score: totalScore)
            Data.Static.saveData()
            break;
        case 2:
            Data.Static.addScore(distance, amount:totalScore)
            Data.Static.addFiredArrows(distance, amount: arrowsFired)
            Data.Static.calculateNewAverage(distance)
            Data.Static.addAndCompareLatestScore(distance, style: style, score: totalScore)
            Data.Static.saveData()
            break;
        default:
            break;
        }
    }
}
