//
//  Data.swift
//  BagskytteRaknevark
//
//  Created by dronnefjord on 2015-04-24.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

import UIKit

class Data: NSObject {
    struct Static {
        
        static let defaults = NSUserDefaults.standardUserDefaults()
        
        static let styles: [String] = ["Fria pil", "30-Serie", "60-Serie"]
        static let distances: [Int] = [18,50,70,90]
        static let scores: [Int] = [0,1,2,3,4,5,6,7,8,9,10]
        
        static var top30: [Int] = [0,0,0,0]
        static var latest30: [Int] = [0,0,0,0]
        static var top60: [Int] = [0,0,0,0]
        static var latest60: [Int] = [0,0,0,0]
        static var arrowAverage: [Double] = [0,0,0,0]
        static var arrowsFired: [Int] = [0,0,0,0]
        static var totalScore: [Int] = [0,0,0,0]
        
        static func addScore(distance:Int, amount:Int){
            totalScore[distance] += amount
        }
        static func addFiredArrows(distance:Int, amount:Int){
            arrowsFired[distance] += amount
        }
        static func calculateNewAverage(distance:Int){
            arrowAverage[distance] = Double(totalScore[distance])/Double(arrowsFired[distance])
        }
        static func addAndCompareLatestScore(distance:Int, style:Int, score:Int){
            switch(style){
            case 1:
                latest30[distance] = score
                if(top30[distance] < score){
                    top30[distance] = score
                }
                break;
            case 2:
                latest60[distance] = score
                if(top60[distance] < score){
                    top60[distance] = score
                }
                break;
            default: break;
            }
        }
        
        static func saveData(){
            /* -- Save total scores -- */
            defaults.setValue(totalScore[0], forKey: "Total18")
            defaults.setValue(totalScore[1], forKey: "Total50")
            defaults.setValue(totalScore[2], forKey: "Total70")
            defaults.setValue(totalScore[3], forKey: "Total90")
            /* -- Save arrows fired -- */
            defaults.setValue(arrowsFired[0], forKey: "Fired18")
            defaults.setValue(arrowsFired[1], forKey: "Fired50")
            defaults.setValue(arrowsFired[2], forKey: "Fired70")
            defaults.setValue(arrowsFired[3], forKey: "Fired90")
            /* -- Save average data -- */
            defaults.setValue(arrowAverage[0], forKey: "ArrowAverage18")
            defaults.setValue(arrowAverage[1], forKey: "ArrowAverage50")
            defaults.setValue(arrowAverage[2], forKey: "ArrowAverage70")
            defaults.setValue(arrowAverage[3], forKey: "ArrowAverage90")
            /* -- Save top data -- */
            defaults.setValue(top30[0], forKey: "Top3018")
            defaults.setValue(top30[1], forKey: "Top3050")
            defaults.setValue(top30[2], forKey: "Top3070")
            defaults.setValue(top30[3], forKey: "Top3090")
            defaults.setValue(top60[0], forKey: "Top6018")
            defaults.setValue(top60[1], forKey: "Top6050")
            defaults.setValue(top60[2], forKey: "Top6070")
            defaults.setValue(top60[3], forKey: "Top6090")
            /* -- Save latest data -- */
            defaults.setValue(latest30[0], forKey: "Latest3018")
            defaults.setValue(latest30[1], forKey: "Latest3050")
            defaults.setValue(latest30[2], forKey: "Latest3070")
            defaults.setValue(latest30[3], forKey: "Latest3090")
            defaults.setValue(latest60[0], forKey: "Latest6018")
            defaults.setValue(latest60[1], forKey: "Latest6050")
            defaults.setValue(latest60[2], forKey: "Latest6070")
            defaults.setValue(latest60[3], forKey: "Latest6090")
            
            /* -- Synch UserDefaults -- */
            defaults.synchronize()
        }
        
        static func loadData(){
            /* -- Load total scores -- */
            if let totalScore18: AnyObject = defaults.valueForKey("Total18"){
                totalScore[0] = totalScore18 as Int
            } else {
                totalScore[0] = 0
            }
            if let totalScore50: AnyObject = defaults.valueForKey("Total50"){
                totalScore[1] = totalScore50 as Int
            } else {
                totalScore[1] = 0
            }
            if let totalScore70: AnyObject = defaults.valueForKey("Total70"){
                totalScore[2] = totalScore70 as Int
            } else {
                totalScore[2] = 0
            }
            if let totalScore90: AnyObject = defaults.valueForKey("Total90"){
                totalScore[3] = totalScore90 as Int
            } else {
                totalScore[3] = 0
            }
            /* -- Load arrows fired -- */
            if let arrowsFired18: AnyObject = defaults.valueForKey("Fired18"){
                arrowsFired[0] = arrowsFired18 as Int
            } else {
                arrowsFired[0] = 0
            }
            if let arrowsFired50: AnyObject = defaults.valueForKey("Fired50"){
                arrowsFired[1] = arrowsFired50 as Int
            } else {
                arrowsFired[1] = 0
            }
            if let arrowsFired70: AnyObject = defaults.valueForKey("Fired70"){
                arrowsFired[2] = arrowsFired70 as Int
            } else {
                arrowsFired[2] = 0
            }
            if let arrowsFired90: AnyObject = defaults.valueForKey("Fired90"){
                arrowsFired[3] = arrowsFired90 as Int
            } else {
                arrowsFired[3] = 0
            }
            /* -- Load average data -- */
            if let arrowAverage18: AnyObject = defaults.valueForKey("ArrowAverage18"){
                arrowAverage[0] = arrowAverage18 as Double
            } else {
                arrowAverage[0] = 0.0
            }
            if let arrowAverage50: AnyObject = defaults.valueForKey("ArrowAverage50"){
                arrowAverage[1] = arrowAverage50 as Double
            } else {
                arrowAverage[1] = 0.0
            }
            if let arrowAverage70: AnyObject = defaults.valueForKey("ArrowAverage70"){
                arrowAverage[2] = arrowAverage70 as Double
            } else {
                arrowAverage[2] = 0.0
            }
            if let arrowAverage90: AnyObject = defaults.valueForKey("ArrowAverage90"){
                arrowAverage[3] = arrowAverage90 as Double
            } else {
                arrowAverage[3] = 0.0
            }
            /* -- Load top data -- */
            if let top3018: AnyObject = defaults.valueForKey("Top3018"){
                top30[0] = top3018 as Int
            } else {
                top30[0] = 0
            }
            if let top3050: AnyObject = defaults.valueForKey("Top3050"){
                top30[1] = top3050 as Int
            } else {
                top30[1] = 0
            }
            if let top3070: AnyObject = defaults.valueForKey("Top3070"){
                top30[2] = top3070 as Int
            } else {
                top30[2] = 0
            }
            if let top3090: AnyObject = defaults.valueForKey("Top3090"){
                top30[3] = top3090 as Int
            } else {
                top30[3] = 0
            }
            if let top6018: AnyObject = defaults.valueForKey("Top6018"){
                top60[0] = top6018 as Int
            } else {
                top60[0] = 0
            }
            if let top6050: AnyObject = defaults.valueForKey("Top6050"){
                top60[1] = top6050 as Int
            } else {
                top60[1] = 0
            }
            if let top6070: AnyObject = defaults.valueForKey("Top6070"){
                top60[2] = top6070 as Int
            } else {
                top60[2] = 0
            }
            if let top6090: AnyObject = defaults.valueForKey("Top6090"){
                top60[3] = top6090 as Int
            } else {
                top60[3] = 0
            }
            /* -- Load latest data -- */
            if let latest3018: AnyObject = defaults.valueForKey("Latest3018"){
                latest30[0] = latest3018 as Int
            } else {
                latest30[0] = 0
            }
            if let latest3050: AnyObject = defaults.valueForKey("Latest3050"){
                latest30[1] = latest3050 as Int
            } else {
                latest30[1] = 0
            }
            if let latest3070: AnyObject = defaults.valueForKey("Latest3070"){
                latest30[2] = latest3070 as Int
            } else {
                latest30[2] = 0
            }
            if let latest3090: AnyObject = defaults.valueForKey("Latest3090"){
                latest30[3] = latest3090 as Int
            } else {
                latest30[3] = 0
            }
            if let latest6018: AnyObject = defaults.valueForKey("Latest6018"){
                latest60[0] = latest6018 as Int
            } else {
                latest60[0] = 0
            }
            if let latest6050: AnyObject = defaults.valueForKey("Latest6050"){
                latest60[1] = latest6050 as Int
            } else {
                latest60[1] = 0
            }
            if let latest6070: AnyObject = defaults.valueForKey("Latest6070"){
                latest60[2] = latest6070 as Int
            } else {
                latest60[2] = 0
            }
            if let latest6090: AnyObject = defaults.valueForKey("Latest6090"){
                latest60[3] = latest6090 as Int
            } else {
                latest60[3] = 0
            }
        }
    }
}
