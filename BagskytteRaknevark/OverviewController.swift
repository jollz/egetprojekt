//
//  OverviewController.swift
//  BagskytteRaknevark
//
//  Created by dronnefjord on 2015-04-24.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

import UIKit

class OverviewController: UIViewController {

    @IBOutlet weak var latest3018: UILabel!
    @IBOutlet weak var latest3050: UILabel!
    @IBOutlet weak var latest3070: UILabel!
    @IBOutlet weak var latest3090: UILabel!
    @IBOutlet weak var top3018: UILabel!
    @IBOutlet weak var top3050: UILabel!
    @IBOutlet weak var top3070: UILabel!
    @IBOutlet weak var top3090: UILabel!
    @IBOutlet weak var latest6018: UILabel!
    @IBOutlet weak var latest6050: UILabel!
    @IBOutlet weak var latest6070: UILabel!
    @IBOutlet weak var latest6090: UILabel!
    @IBOutlet weak var top6018: UILabel!
    @IBOutlet weak var top6050: UILabel!
    @IBOutlet weak var top6070: UILabel!
    @IBOutlet weak var top6090: UILabel!
    @IBOutlet weak var arrowAverage18: UILabel!
    @IBOutlet weak var arrowAverage50: UILabel!
    @IBOutlet weak var arrowAverage70: UILabel!
    @IBOutlet weak var arrowAverage90: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        loadData()
    }
    @IBAction func clearSavedData(sender: AnyObject) {
        var alert = UIAlertController(title: "Radera data", message: "Det här raderar samtlig sparad data.", preferredStyle: UIAlertControllerStyle.ActionSheet)
        alert.addAction(UIAlertAction(title: "Avbryt", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Radera", style: UIAlertActionStyle.Destructive, handler: { action in
            var appDomain = NSBundle.mainBundle().bundleIdentifier
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain!)
            self.loadData()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadData(){
        /* -- Load locally stored data -- */
        Data.Static.loadData()
        
        /** --- Update labels with loaded data --- **/
        /* -- Latest 30 -- */
        latest3018.text = String(Data.Static.latest30[0])
        latest3050.text = String(Data.Static.latest30[1])
        latest3070.text = String(Data.Static.latest30[2])
        latest3090.text = String(Data.Static.latest30[3])
        
        /* -- Top 30 -- */
        top3018.text = String(Data.Static.top30[0])
        top3050.text = String(Data.Static.top30[1])
        top3070.text = String(Data.Static.top30[2])
        top3090.text = String(Data.Static.top30[3])
        
        /* -- Latest 60 -- */
        latest6018.text = String(Data.Static.latest60[0])
        latest6050.text = String(Data.Static.latest60[1])
        latest6070.text = String(Data.Static.latest60[2])
        latest6090.text = String(Data.Static.latest60[3])
        
        /* -- Top 60 -- */
        top6018.text = String(Data.Static.top60[0])
        top6050.text = String(Data.Static.top60[1])
        top6070.text = String(Data.Static.top60[2])
        top6090.text = String(Data.Static.top60[3])
        
        /* -- Arrow Average -- */
        arrowAverage18.text = NSString(format: "%.1f", Data.Static.arrowAverage[0])
        arrowAverage50.text = NSString(format: "%.1f", Data.Static.arrowAverage[1])
        arrowAverage70.text = NSString(format: "%.1f", Data.Static.arrowAverage[2])
        arrowAverage90.text = NSString(format: "%.1f", Data.Static.arrowAverage[3])
    }
}
