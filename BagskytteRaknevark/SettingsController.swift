//
//  SettingsController.swift
//  BagskytteRaknevark
//
//  Created by dronnefjord on 2015-04-24.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

import UIKit

class SettingsController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var stylePickerView: UIPickerView!
    @IBOutlet weak var distancePickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylePickerView.delegate = self
        stylePickerView.dataSource = self
        stylePickerView.tag = 0
        distancePickerView.delegate = self
        distancePickerView.dataSource = self
        distancePickerView.tag = 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 0){
            return Data.Static.styles.count
        } else {
            return Data.Static.distances.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if(pickerView.tag == 1){
            return NSString(format: "%dm", Data.Static.distances[row])
        } else {
            return Data.Static.styles[row]
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var destination = segue.destinationViewController as RegistrationController
        destination.style = stylePickerView.selectedRowInComponent(0)
        destination.distance = distancePickerView.selectedRowInComponent(0)
    }
}
